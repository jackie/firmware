#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include "max86150.h"
#include "epicardium.h"
#include "modules.h"
#include "modules/log.h"
#include "modules/stream.h"
#include "gpio.h"
#include "pmic.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

#include "api/interrupt-sender.h"
#include "modules/modules.h"

#define LOCK_WAIT pdMS_TO_TICKS(MAX86150_MUTEX_WAIT_MS)

static const gpio_cfg_t max86150_interrupt_pin = {
	PORT_1, PIN_13, GPIO_FUNC_IN, GPIO_PAD_PULL_UP
};

/* MAX86150 Task ID */
static TaskHandle_t max86150_task_id = NULL;

/* MAX86150 Mutex */
static StaticSemaphore_t max86150_mutex_data;
static SemaphoreHandle_t max86150_mutex = NULL;

/* Stream */
static struct stream_info max86150_stream;

/* Active */
static bool max86150_sensor_active = false;

int epic_max86150_enable_sensor(void)
{
	int result = 0;

	result = hwlock_acquire(HWLOCK_I2C, pdMS_TO_TICKS(100));
	if (result < 0) {
		return result;
	}

	if (xSemaphoreTake(max86150_mutex, LOCK_WAIT) != pdTRUE) {
		result = -EBUSY;
		goto out_free_i2c;
	}

	struct stream_info *stream = &max86150_stream;
	stream->item_size = sizeof(struct max86150_sensor_data);
	stream->queue = xQueueCreate(128, stream->item_size);
	if (stream->queue == NULL) {
		result = -ENOMEM;
		goto out_free_both;
	}

	result = stream_register(SD_MAX86150, stream);
	if (result < 0) {
		vQueueDelete(stream->queue);
		goto out_free_both;
	}

	result = max86150_begin();
	if (result < 0) {
		vQueueDelete(stream->queue);
		goto out_free_both;
	}
	max86150_setup();
	max86150_get_int1();
	max86150_get_int2();

	max86150_sensor_active = true;
	result                 = SD_MAX86150;

out_free_both:
	xSemaphoreGive(max86150_mutex);
out_free_i2c:
	hwlock_release(HWLOCK_I2C);
	return result;
}

int epic_max86150_disable_sensor(void)
{
	int result = 0;

	result = hwlock_acquire(HWLOCK_I2C, pdMS_TO_TICKS(100));
	if (result < 0) {
		return result;
	}

	if (xSemaphoreTake(max86150_mutex, LOCK_WAIT) != pdTRUE) {
		result = -EBUSY;
		goto out_free_i2c;
	}

	struct stream_info *stream = &max86150_stream;
	result                     = stream_deregister(SD_MAX86150, stream);
	if (result < 0) {
		goto out_free_both;
	}

	vQueueDelete(stream->queue);
	stream->queue = NULL;

	max86150_sensor_active = false;

	result = 0;
out_free_both:
	xSemaphoreGive(max86150_mutex);
out_free_i2c:
	hwlock_release(HWLOCK_I2C);
	return result;
}

static void max86150_handle_sample(struct max86150_sensor_data *data)
{
	//LOG_INFO("max86150", "Sample! %ld, %ld, %ld", data->red, data->ir, data->ecg);

	if (max86150_stream.queue == NULL) {
		return;
	}

	if (xQueueSend(max86150_stream.queue, data, MAX86150_MUTEX_WAIT_MS) != pdTRUE) {
		LOG_WARN("max86150", "queue full"); // TODO; handle queue full
	}
	api_interrupt_trigger(EPIC_INT_MAX86150);
}

void epic_max86150_set_led_amplitude(uint8_t red, uint8_t ir)
{
	max86150_set_led_red_amplitude(red);
	max86150_set_led_ir_amplitude(ir);
}

static int max86150_fetch_fifo(void)
{
	int result = 0;

	result = hwlock_acquire(HWLOCK_I2C, pdMS_TO_TICKS(100));
	if (result < 0) {
		return result;
	}

	if (xSemaphoreTake(max86150_mutex, LOCK_WAIT) != pdTRUE) {
		result = -EBUSY;
		goto out_free_i2c;
	}

	struct max86150_sensor_data sample;

	// Check if PPG or ECG data is available
	//int num = max86150_check();
	//LOG_INFO("max86150", "Check: %d", num);

	//LOG_INFO("max86150", "WP: %d, RP: %d B", max86150_get_fifo_write_pointer(), max86150_get_fifo_read_pointer());

	max86150_set_int_full(false);

	//while (max86150_available() > 0) {
	while (max86150_get_sample(&(sample.red), &(sample.ir), &(sample.ecg)) > 0) {

		//LOG_INFO("max86150", "WP: %d, RP: %d L", max86150_get_fifo_write_pointer(), max86150_get_fifo_read_pointer());
		//LOG_INFO("max86150", "Sample Available");
		//sample.red = max86150_get_fifo_red();
		//sample.ir = max86150_get_fifo_ir();
		//sample.ecg = max86150_get_fifo_ecg();
		//max86150_next_sample();
		max86150_handle_sample(&sample);
	}
	
	max86150_get_int1();
	//max86150_get_int2();
	
	max86150_set_int_full(true);

	xSemaphoreGive(max86150_mutex);
out_free_i2c:
	hwlock_release(HWLOCK_I2C);
	return result;
}

/*
 * Callback for the MAX86150 interrupt pin.  This callback is called from the
 * SDK's GPIO interrupt driver, in interrupt context.
 */
static void max86150_interrupt_callback(void *_)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (max86150_task_id != NULL) {
		vTaskNotifyGiveFromISR(
			max86150_task_id, &xHigherPriorityTaskWoken
		);
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
}
/* }}} */

void vMAX86150Task(void *pvParameters)
{
	max86150_task_id =	xTaskGetCurrentTaskHandle();
	max86150_mutex =	xSemaphoreCreateMutexStatic(&max86150_mutex_data);

	int lockret = hwlock_acquire(HWLOCK_I2C, pdMS_TO_TICKS(100));
	if (lockret < 0) {
		LOG_CRIT("max86150", "Failed to acquire I2C lock!");
		vTaskDelay(portMAX_DELAY);
	}

	/* Take Mutex during initialization, just in case */
	if (xSemaphoreTake(max86150_mutex, 0) != pdTRUE) {
		LOG_CRIT("max86150", "Failed to acquire MAX86150 mutex!");
		vTaskDelay(portMAX_DELAY);
	}

	/* Install interrupt callback */
	GPIO_Config(&max86150_interrupt_pin);
	GPIO_RegisterCallback(
		&max86150_interrupt_pin, max86150_interrupt_callback, NULL
	);
	GPIO_IntConfig(
		&max86150_interrupt_pin, GPIO_INT_EDGE, GPIO_INT_FALLING
	);
	GPIO_IntEnable(&max86150_interrupt_pin);
	NVIC_SetPriority(
		(IRQn_Type)MXC_GPIO_GET_IRQ(max86150_interrupt_pin.port), 2
	);
	NVIC_EnableIRQ(
		(IRQn_Type)MXC_GPIO_GET_IRQ(max86150_interrupt_pin.port)
	);

	xSemaphoreGive(max86150_mutex);
	hwlock_release(HWLOCK_I2C);

	/* ----------------------------------------- */

	while (1) {
		if (max86150_sensor_active) {
			//LOG_INFO("max86150", "Interrupt!");
			int ret = max86150_fetch_fifo();
			if (ret == -EBUSY) {
				LOG_WARN("max86150", "Could not acquire mutex?");
				continue;
			} else if (ret < 0) {
				LOG_ERR("max86150", "Unknown error: %d", -ret);
			}
		}
		/*
		 * Wait for interrupt.  After two seconds, fetch FIFO anyway
		 *
		 * In the future, reads using epic_stream_read() might also
		 * trigger a FIFO fetch, from outside this task.
		 */
		ulTaskNotifyTake(pdTRUE, pdMS_TO_TICKS(2000));
	}
}
